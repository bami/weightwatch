package info.migge.weightwatch.models;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * storage container weight
 * <p/>
 * Created by Bastian Migge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class Weight {
    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    private Float weight;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    private long timestamp;


    public void setWeight(float weight) {
        this.weight = Float.valueOf(weight);
    }

    /**
     * create object with given weight and current time
     * @param weight
     */
    public Weight(float weight) {
        Date now = Calendar.getInstance().getTime();
        initialize(weight, now);
    }

    /**
     * create object with given weight and timestamp
     * @param weight
     * @param timestamp
     */
    public Weight(float weight, Date timestamp) {
        initialize(weight, timestamp);
    }

    private void initialize(float weight, Date timestamp) {
        setWeight(weight);
        this.timestamp = timestamp.getTime();
    }

    /**
     * return weight value as string
     * @return
     */
    public String getWeightString() {
        return this.weight.toString();
    }

    /**
     * return timestamp as string
     * @return
     */
    public String getTimeStampString() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String dateString = formatter.format(new Date(this.timestamp));
        return dateString;
    }

    @Override
    public String toString() {
        return toJSON().toString();
    }

    /**
     * returns JSON representation of object
     * @return
     */
    public JSONObject toJSON() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("weight", this.getWeightString());
            jsonObj.put("timestamp", this.getTimeStampString());

        } catch (JSONException e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return jsonObj;
    }
}