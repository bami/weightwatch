package info.migge.weightwatch.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import info.migge.weightwatch.R;

/**
 *
 * WeightListAdapter
 * <p/>
 * adapter between weight model and list view
 *
 * Created by Bastian Migge on 09/03/15.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class WeightListAdapter extends BaseAdapter {

    Context context;

    protected List<Weight> listWeights;
    LayoutInflater inflater;

    public WeightListAdapter(Context context, List<Weight> listWeights) {
        this.listWeights = listWeights;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    public int getCount() {
        return listWeights.size();
    }

    public Weight getItem(int position) {
        return listWeights.get(position);
    }

    public long getItemId(int position) {
        return listWeights.get(position).getTimestamp();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {

            holder = new ViewHolder();
            convertView = this.inflater.inflate(R.layout.layout_list_item,
                    parent, false);

            holder.tvWeight = (TextView) convertView
                    .findViewById(R.id.tv_weight);
            holder.tvTimestamp = (TextView) convertView
                    .findViewById(R.id.tv_timestamp);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Weight weight = listWeights.get(position);
        holder.tvWeight.setText(weight.getWeightString());
        holder.tvTimestamp.setText(weight.getTimeStampString());

        return convertView;
    }

    private class ViewHolder {
        TextView tvWeight;
        TextView tvTimestamp;
    }

}