package info.migge.weightwatch;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import info.migge.weightwatch.models.Weight;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.Utils;
import lecho.lib.hellocharts.view.Chart;
import lecho.lib.hellocharts.view.LineChartView;

/**
 *
 * Weight chart activity
 * <p/>
 * Created by Bastian Migge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class ChartActivity extends ActionBarActivity {
    private LineChartView chart;
    private LineChartData data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_chart);
        chart = (LineChartView) findViewById(R.id.chart);
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadData();

        // Disable viewport recalculations, see toggleCubic() method for more info.
        chart.setViewportCalculationEnabled(false);

        resetViewport();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_list) {
            Intent myIntent = new Intent(ChartActivity.this, ListActivity.class);
            startActivity(myIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Adapt viewport height range to data
     */
    private void resetViewport() {
        final Viewport v = new Viewport(chart.getMaximumViewport());

        float y;
        float minY = Float.POSITIVE_INFINITY;
        for (Line l : data.getLines()) {
            for (PointValue p : l.getValues()) {
                y = p.getY();
                if (y < minY) {
                    minY = y;
                }
            }
        }
        v.bottom = minY - 1;

        // get maxY
        float maxY = Float.NEGATIVE_INFINITY;
        for (Line line : data.getLines()) {
            for (PointValue value : line.getValues()) {
                y = value.getY();
                if (y > maxY) {
                    maxY = y;
                }
            }
        }
        v.top = maxY + 1;

        chart.setMaximumViewport(v);
        chart.setCurrentViewport(v, true);
    }

    /**
     * load chart data from storage
     */
    private void loadData() {
        List<Line> lines = new ArrayList<Line>();

        ArrayList<Weight> weightList = Storage.loadWeightList(this.getApplicationContext());

        List<PointValue> values = new ArrayList<PointValue>();
        for (int j = 0; j < weightList.size(); ++j) {
            Weight e = weightList.get(j);
            values.add(0, new PointValue(e.getTimestamp(), e.getWeight()));
        }

        Line line = new Line(values);
        line.setColor(Utils.COLOR_GREEN);
        line.setShape(ValueShape.CIRCLE);
        line.setCubic(false);
        line.setFilled(false);
        line.setHasLabels(false);
        line.setHasLabelsOnlyForSelected(false);
        line.setHasLines(true);
        line.setHasPoints(false);
        lines.add(line);

        data = new LineChartData(lines);

        Axis axisX = new Axis();
        Axis axisY = new Axis().setHasLines(true);

        axisX.setName(getString(R.string.time));
        axisY.setName(getString(R.string.weight));

        data.setAxisXBottom(null);
        data.setAxisYLeft(axisY);

        data.setBaseValue(Float.NEGATIVE_INFINITY);
        chart.setLineChartData(data);
    }

    /**
     * To animate values you have to change targets values and then call {@link Chart#startDataAnimation()}
     * method(don't confuse with View.animate()). If you operate on data that was set before you don't have to call
     * {@link LineChartView#setLineChartData(LineChartData)} again.
     */
    private void prepareDataAnimation() {
        for (Line line : data.getLines()) {
            for (PointValue value : line.getValues()) {
                // Here I modify target only for Y values but it is OK to modify X targets as well.
                value.setTarget(value.getX(), (float) Math.random() * 100);
            }
        }
    }
}
