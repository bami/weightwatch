package info.migge.weightwatch;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import info.migge.weightwatch.models.Weight;

/**
 *
 * Single storage center
 * <p/>
 * Created by Bastian Migge on 08/03/15.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
public class Storage {

    static SharedPreferences appSharedPrefs;
    static SharedPreferences.Editor editor;

    /**
     * initialized storage
     *
     * @param appContext application context
     */
    protected static void initialize(Context appContext) {
        if (appSharedPrefs == null) {
            appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(appContext);
        }

        if (editor == null) {
            editor = appSharedPrefs.edit();
        }
    }

    /**
     * Load mWeightList from persistance
     *
     * @param appContext application context
     * @return
     */
    public static ArrayList<Weight> loadWeightList(Context appContext) {

        initialize(appContext);

        ArrayList<Weight> weightList = new ArrayList<Weight>();

        Gson gson = new Gson();
        String jsonEntries = appSharedPrefs.getString("entries", null);

        if (jsonEntries == null) {
            weightList = new ArrayList<Weight>();
        } else {
            Type type = new TypeToken<List<Weight>>() {
            }.getType();
            weightList = gson.fromJson(jsonEntries, type);
        }
        return weightList;
    }

    /**
     * Make weightlist presistant
     *
     * @param weightList List of Entries
     */
    public static void storeWeightList(Context appContext, ArrayList<Weight> weightList) {

        initialize(appContext);

        Gson gson = new Gson();
        String json = gson.toJson(weightList);
        Log.d("lala", json);
        try {
            editor.putString("entries", json);
            editor.apply();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static void wipeWeightList(Context appContext) {

        initialize(appContext);

        Gson gson = new Gson();
        String json = gson.toJson(new ArrayList<Weight>());
        editor.putString("entries", json);
        editor.apply();
    }

    public static void storeDummyData(Context appContext) {

        initialize(appContext);

        ArrayList<Weight> weightList = new ArrayList<Weight>(10);


        // 88.0 10.3. 07:20
        Weight e = new Weight(88.0f, new Date(2015 - 1900, 3 - 1, 10, 7, 20));
        weightList.add(e);

        // 89.0 9.3. 22:47
        e = new Weight(89.0f, new Date(2015 - 1900, 3 - 1, 9, 22, 47));
        weightList.add(e);


        // 87.0 9.3. 07:28
        e = new Weight(87.0f, new Date(2015 - 1900, 3 - 1, 9, 7, 28));
        weightList.add(e);

        // 87.4 8.3. 23:20
        e = new Weight(87.4f, new Date(2015 - 1900, 3 - 1, 8, 23, 20));
        weightList.add(e);


        // 86.9 6.3. 07:18
        e = new Weight(86.9f, new Date(2015 - 1900, 3 - 1, 6, 7, 18));
        weightList.add(e);


        // 88,2 5.3. 21:05
        e = new Weight(88.2f, new Date(2015 - 1900, 3 - 1, 5, 21, 5));
        weightList.add(e);

        // 89,5 4.3. 22:42
        e = new Weight(89.5f, new Date(2015 - 1900, 3 - 1, 4, 22, 42));
        weightList.add(e);

        // 86,9 4.3. 09:09
        e = new Weight(86.9f, new Date(2015 - 1900, 3 - 1, 4, 9, 9));
        weightList.add(e);


        //88.2 3.3.2015 22:49
        e = new Weight(88.2f, new Date(2015 - 1900, 3 - 1, 3, 22, 49));
        weightList.add(e);

        storeWeightList(appContext, weightList);
    }

}
