package info.migge.weightwatch;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import info.migge.weightwatch.models.Weight;
import info.migge.weightwatch.models.WeightListAdapter;

/**
 *
 * List of weights activity
 * <p/>
 * Created by Bastian Migge
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */
public class ListActivity extends ActionBarActivity {

    public static final String tag = "ListActivity";

    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor editor;

    private ArrayList<Weight> mWeightList;
    private WeightListAdapter adapter;

    private ListView listView;
    private Button addButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Context context = this;

        // init data
        mWeightList = new ArrayList<Weight>();

        // init view
        setContentView(R.layout.activity_list);
        setTitle(getString(R.string.title_activity_list));
        listView = (ListView) findViewById(R.id.list);
        adapter = new WeightListAdapter(this, mWeightList);
        listView.setAdapter(adapter);

        // ListView Item Click Listener Delete
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final Weight weight = (Weight) listView.getItemAtPosition(position);

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.dialog_delete_weight);
                builder.setMessage(weight.toString())
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // ListView Clicked item value
                                mWeightList.remove(weight);
                                adapter.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                // Create the AlertDialog object
                AlertDialog dialog = builder.create();
                dialog.show();

                return true;
            }
        });

        // ListView Item Click Listener Edit
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item value
                final Weight weight = (Weight) listView.getItemAtPosition(position);

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_entry);
                dialog.setTitle(R.string.dialog_edit_weight);

                final EditText text = (EditText) dialog.findViewById(R.id.weight);
                text.setText(String.valueOf(weight.getWeightString()));
                Button dialogButtonOk = (Button) dialog
                        .findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom dialog
                dialogButtonOk.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        try {
                            final String value = text.getText().toString();
                            float f = Float.valueOf(value).floatValue();
                            weight.setWeight(f);
                            refreshView();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Toast.makeText(context, "Invalid value is ignored.", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                });

                Button dialogButtonCancel = (Button) dialog
                        .findViewById(R.id.dialogButtonCancel);
                // if button is clicked, cancel the custom dialog
                dialogButtonCancel.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                dialog.show();
            }

        });

        addButton = (Button) findViewById(R.id.addButton);

        // add button listener
        addButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // custom dialog
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_entry);
                dialog.setTitle(R.string.dialog_edit_weight);

                final EditText text = (EditText) dialog.findViewById(R.id.weight);

                Button dialogButtonOk = (Button) dialog
                        .findViewById(R.id.dialogButtonOK);
                // if button is clicked, close the custom dialog
                dialogButtonOk.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        try {
                            final String value = text.getText().toString();
                            float f = Float.valueOf(value).floatValue();
                            mWeightList.add(0, new Weight(f));
                            refreshView();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Toast.makeText(context, "Invalid value is ignored.", Toast.LENGTH_SHORT).show();
                        }
                        dialog.dismiss();
                    }
                });

                Button dialogButtonCancel = (Button) dialog
                        .findViewById(R.id.dialogButtonCancel);
                // if button is clicked, cancel the custom dialog
                dialogButtonCancel.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadData();
        refreshView();
    }

    @Override
    protected void onPause() {
        Storage.storeWeightList(this.getApplicationContext(), mWeightList);

        super.onPause();
    }

    /**
     * load data from storage
     */
    protected void loadData() {
        // refresh list
        mWeightList.clear();

        //Storage.storeDummyData(this.getApplicationContext());
        ArrayList<Weight> weightList = Storage.loadWeightList(this.getApplicationContext());
        mWeightList.addAll(weightList);
    }

    /**
     * refresh view
     */
    protected void refreshView() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_chart) {
            Intent myIntent = new Intent(ListActivity.this, ChartActivity.class);
            startActivity(myIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}