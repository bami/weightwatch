# README #

This README documents what WeightWatch is and how you use it.


### What is WeightWatch for ? ###

* Monitor your weight over time on your android device

### How do I use WeightWatch ? ###

* Install app on android phone
* Add weight data (list view)
* View development of weight as line graph (graph view)

### Application package ###

[install WeightWatch](https://bitbucket.org/bami/weightwatch/raw/master/
app/WeightWatch_0.1.apk)

#### List view ####
![IMAGE](https://bitbucket.org/bami/weightwatch/raw/master/doc/list.png)

#### Graph view ####
![IMAGE](https://bitbucket.org/bami/weightwatch/raw/master/doc/graph.png)

### Used libs ###

* [hellochart graph library](https://github.com/lecho/hellocharts-android)
* [gson library](https://code.google.com/p/google-gson/)
